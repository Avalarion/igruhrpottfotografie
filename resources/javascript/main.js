var manually = false;
$(document).ready(function() {
	bindMenu();
	bindNavigation();
	setInterval(function(){if(!manually) showNext();}, 7500);
});

function bindMenu() {
	$('.main').click(function(e){
		$('.contents').removeClass('onlyBackground');
		$('.cur').removeClass('cur');
		$(e.target).addClass('cur');
		$('.display').removeClass('display');
	});
	$('#who, #dates, #contact').click(function(e){
		$('.contents').addClass('onlyBackground');
		$('.cur').removeClass('cur');
		$(e.target).addClass('cur');
		$('.display').removeClass('display');
		$('.'+$(e.target).attr('id')).addClass('display');
	});
}

function bindNavigation() {
	var items = $('#backgrounds article');
	var item = items[Math.floor(Math.random()*items.length)];
	$(item).addClass('act');
	$('.next').click(function(){manually = true; showNext();});
	$('.prev').click(function(){manually = true; showPrev();});
}

function showNext() {
	var tmp = $('.act').next('article');
	if(tmp.length == 0) tmp = $('#backgrounds article').first();
	switchImage(tmp, $('.act'));
}

function showPrev() {
	var tmp = $('.act').prev('article');
	if(tmp.length == 0) tmp = $('#backgrounds article').last();
	switchImage(tmp, $('.act'));
}

function switchImage(newImage, oldImage) {
	$(oldImage).fadeOut('slow', function(){	$(oldImage).removeClass('act'); });
	$(newImage).fadeIn('slow', function(){ $(newImage).addClass('act'); });
}
