<!DOCTYPE html>

<html>
	<head>
		<title>IG Ruhrpottfotografie - Die Fotogruppe ausm Pott</title>	

		<meta charset="UTF-8">

		<meta http-equiv="X-UA-Compatible" content="IE=EDGE">
		<link rel="stylesheet" href="resources/css/main.css" type="text/css">
		<link rel="icon" href="resources/images/favicon.png">
		<script type="text/javascript" src="resources/javascript/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="resources/javascript/main.js"></script>

		<meta property="og:title" content="IG Ruhrpottfotografie - Die Fotogruppe ausm Pott" />
		<meta property="og:description" content="Wir sind eine stetig wachsende Gruppe von ambitionierten Fotografen aus dem Pott, im Pott und um den Pott herum. Du möchtest Teil von uns sein? Komm doch einfach beim nächsten Treffen ins UPH!" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://www.ig-ruhrpottfotografie.de" />
		<meta property="og:image" content="http://www.ig-ruhrpottfotografie.de/resources/images/logo.png" />
		<meta property="og:site_name" content="ig-ruhrpottfotografie.de" />
		<meta property="fb:admins" content="bastian.bringenberg" />
	</head>
	<body>
		<div id="content">
			<header>
				<div class="logo"></div>
				<nav>
					<span class="" id="who">Info</span>
					<span class="main cur">Wer sind wir?</span>
					<span class="" id="dates">Termine</span>
					<span class="" id="contact">Kontakt</span>
					<a class="fb" target="_blank" href="https://www.facebook.com/groups/igruhrpottfotografie/">Facebook</a>
				</nav>
			</header>

			<div class="contents">
				<div id="backgrounds">
					<div class="prev">&lt;</div>

					<article class="torsten_thies">
						<img src="authors/torsten_thies.jpg" alt=""/>
						<section style="margin-top: 370px; margin-left: 25px; width: 500px;">
							<h2>Torsten Thies</h2>
							<p>Als Initiator und Gründer der Fotogruppe IGR hatte er eigentlich nur vier Dinge für die Gruppe im Kopf:
								Füreinander organisieren, miteinander fotografieren, voneinander lernen und den Ruhrpott mit anderen Augen sehen!</p>

							<p>Im Focus seiner Fotografie liegen die Menschen und die Metropole Ruhr!<br />
								Workshops zu diesen Bereichen führt er auch durch!</p>
							<p>Du willst Kontakt zu Torsten haben? Schick' ihm 'ne <a href="mailto:tt@ig-ruhrpottfotografie.de">Mail</a>!</p>
						</section>
					</article>

					<article class="bastian_bringenberg">
						<img src="authors/bastian_bringenberg.jpg" alt=""/>
						<section style="margin-top: 25px; margin-left: 475px;">
							<h2>Bastian Bringenberg</h2>
							<p>Bastian ist schon von Anfang an dabei und kümmert sich um die Technik hinter der IGR. Als Student und Hobbyfotograf sind seine Hauptziele coole Ruinen zu finden und damit seine Heimat besser kennen zu lernen.</p>
							<p>Du willst Kontakt zu Bastian haben? Schick' ihm 'ne <a href="mailto:bastian.bringenberg@ig-ruhrpottfotografie.de">Mail</a>!</p>
						</section>
					</article>

					<article class="carsten_deckert">
						<img src="authors/carsten_deckert.jpg" alt=""/>
						<section style="margin-top: 25px; margin-left: 550px; width: 200px;">
							<h2>Carsten Deckert</h2>
							<p>Carsten ist im Ruhrgebiet groß geworden und verfolgt mit Spannung wie sich der Strukturwandel vollzieht. Es ist einfach spannend, wie viel es in seiner nähren Umgebung zu entdecken gibt. Es ist die Vielfalt, die diese Region so prägt. Dabei hat es Carsten besonders die Architektur- und Nachtfotografie angetan.</p>
							<p>Du willst Kontakt zu Carsten haben? Schick' ihm 'ne <a href="mailto:carsten.deckert@ig-ruhrpottfotografie.de">Mail</a>!</p>
						</section>
					</article>

					<article class="jochen_pils">
						<img src="authors/jochen_pils.jpg" alt=""/>
						<section style="margin-top: 20px; margin-left: 75px;">
							<h2>Jochen Pils</h2>
							<p>Seit 2013 bei der IGR. Zu seinen Schwerpunkten zählen Industriekultur und Menschen.</p>
							<p>Du willst Kontakt zu Jochen haben? Schick' ihm 'ne <a href="mailto:jochen.pils@ig-ruhrpottfotografie.de">Mail</a>!</p>
						</section>
					</article>

					<article class="dirk_walther">
						<img src="authors/dirk_walther.jpg" alt=""/>
						<section style="margin-top: 20px; margin-left: 75px;">
							<h2>Dirk Walther</h2>
							<p>Mitten im Ruhrgebiet aufgewachsen, schätzt Dirk dessen große Vielfalt. Hier bieten sich ihm immer wieder neue Motive, angefangen von alten Industrieanlagen, romantischen Burgen und Schlössern bis hin zur Natur. Seine besondere Leidenschaft gilt der Konzert- und Eventfotografie.</p>
							<p>Du willst Kontakt zu Dirk haben? Schick' ihm 'ne <a href="mailto:dirk.walther@ig-ruhrpottfotografie.de">Mail</a>!</p>
						</section>
					</article>

					<article class="jana_sandner">
						<img src="authors/jana_sandner.jpg" alt=""/>
						<section style="margin-top: 10px; margin-left: 375px; width: 375px;">
							<h2>Jana Sandner</h2>
							<p>Jana ist schon lange Fotografin, aber noch nicht lange im Ruhrpott. Auch wenn sie ganzheitlich lebt, schaut sie gerne ins Detail. Ganz besonders liebt sie roten und rostigen Stahl sowie Geschichten, die sich auf Parkbänken und auf den Straßen abspielen.</p>
							<p>Du willst Kontakt zu Jana haben? Schick' ihr 'ne <a href="mailto:jana_sandner@ig-ruhrpottfotografie.de">Mail</a>!</p>
						</section>
					</article>

					<article class="carsten_schweitzer">
						<img src="authors/carsten_schweitzer.jpg" style="height: 533px;" alt=""/>
						<section style="margin-top: 380px; margin-left: 500px;">
							<h2>Carsten Schweitzer</h2>
							<p>Fotografie ist für Carsten die Auseinandersetzung mit dem Vergänglichen, die Konservierung besonderer Momente und das immer wieder Neuentdecken visueller Wahrnehmungen.</p>
							<p>Du willst Kontakt zu Carsten haben? Schick' ihm 'ne <a href="mailto:carsten_schweitzer@ig-ruhrpottfotografie.de">Mail</a>!</p>
						</section>
					</article>

					<article class="andrea_schwarz">
						<img src="authors/andrea_schwarz.jpg" style="height: 533px;" alt=""/>
						<section style="margin-top: 25px;margin-left: 568px;width: 200px;;">
							<h2>Andrea Schwarz</h2>
							<p>Als gebürtige Altenessenerin liebt sie die Romantik des Ruhrpotts und seine Menschen. </p>
							<p>Fotografische Bereiche: Spiel mit Schärfe/ Unschärfe und Detailaufnahmen. Ihre Leidenschaft jedoch ist die Peoplefotografie. Ob im Studio oder an einer schönen Industriekulisse … Menschen in Szene zu setzen und mit ihnen zu arbeiten macht den Reiz aus. Auch führt sie als Trainerin Anfängerworkshops im Bereich Fotografie durch.</p>
							<p>Du willst Kontakt zu Andrea haben? Schick' ihr 'ne <a href="mailto:andrea_schwarz@ig-ruhrpottfotografie.de">Mail</a>!</p>
						</section>
					</article>

					<div class="next">&gt;</div>
				</div>
				<div id="pages">
					<article id="mainpage">
						<!-- This will remain empty as it is just -->
					</article>
					<article class="who" style="margin-top: 125px; margin-left: 175px; overflow:auto; width: 500px; height: 300px;">
						<h2>Was ist die IG Ruhrpottfotografie</h2>
						<p>Als stetig wachsende Gruppe ambitionierter Fotografen aus dem Pott ist es unser Ziel uns Gegenseitig auf Augenh&ouml;he zu begegnen, Spaß zu haben und einander das ein oder andere &uuml;ber Fotografie oder unseren geliebten Pott beizubringen.</p>
						<p>Dabei ist und vollkommen egal ob unsere Mitglieder jung oder alt, schwarz oder weiß, neu in der Fotografie oder alt eingesessene Hasen sind. Bei uns geht es um Zusammenarbeit.</p>
						<p>Neben Bildpräsentationen und gemeinsamen Bilderausflügen trifft sich die IGR einmal im Monat, für gewöhnlich am 2. Donnerstag, im <a href="http://www.unperfekthaus.de/">Unperfekthaus</a> in Essen. Direkt an dem Limbecker Platz. Fühl dich eingeladen einfach mal vorbei zu schauen! Die genauen Termine findest du unter Termine.</p>
						<p>Immer noch Fragen wer wir sind? Dann meld dich am besten bei <a href="mailto:tt@ig-ruhrpottfotografie.de">Torsten</a> oder <a href="mailto:bastian-bringenberg@ig-ruhrpottfotografie.de">Bastian</a>, komm in unsere Facebook Gruppe oder besuch uns im UPH um dir selbst ein Bild zu machen!</p>
					</article>
					<article class="dates" style="width: 450px; height: 300px; overflow: auto; margin-left: 225px; margin-top: 100px;">
						<h2>Wichtige Termine</h2>
						<p>Du möchtest einen Termin vorschlagen oder an einem Termin teilnehmen?<br /> Schicke doch bitte eine Kurze Information an <a href="mailto:termine@ig-ruhrpottfotografie.de">termine@ig-ruhrpottfotografie.de</a> oder trag deinen Termin in der Facebook Gruppe ein.</p>
						<div class="dates">
						<?php
							require('./facebook_settings.php');
							$link = 'https://graph.facebook.com/217100728359669/events?fields=owner,description,start_time,name,location&since=now&access_token='.$token;
        					$var = file_get_contents($link);
        					$var = json_decode($var,true);
        					$dates = array();
        					$i = 0;
							foreach($var['data'] as $event){
								$event['start_time'] = DateTime::createFromFormat ('Y-m-d*H:i:s', $event['start_time']);
								$dates[($event['start_time']->format(U)+$i)] = $event;
								$i++;
							}
							ksort($dates);
							foreach($dates as $event) {
								echo '<div class="event">';
								echo '<h4>'.$event['name'].'</h4>';
								echo '<address>'.$event['location'].'</address>';
								echo '<time>'.$event['start_time']->format('d.m.Y H:i').'</time>';
								echo '<div class="description">'.$event['description'].'</div>';
								echo '</div>';
							}
						?>
						</div>
					</article>
					<article class="contact" style="max-height: 400px; overflow: auto; margin-left: 75px; margin-top: 35px;">
						<h2>Impressum</h2>
						<p>
							<i>Personelle Administration</i><br />
							<b>Torsten Thies</b><br />
							Moritzstra&szlig;e 2<br />
							45131 Essen<br />
						</p>
						<p>
							Email:	<a href="tt@ig-ruhrpottfotografie.de">tt@ig-ruhrpottfotografie.de</a>
						</p>
						<p>
							<i>Technische Administration</i><br />
							<b>Bastian Bringenberg</b><br />
							Kniestra&szlig;e 1<br />
							46117 Oberhausen<br />
						</p>
						<p>
							Telefon: 0208 811828<br />
							Email:	 <a href="mailto:bastian@ig-ruhrpottfotografie.de">bastian@ig-ruhrpottfotografie.de</a>
						</p>
						<h3>Informationen</h3>
						<p>
							Alle auf dieser Webseite verwendeten Bilder unterliegen ihren Autoren. Eine Verwendung ohne ausdrückliche Genehmigung des Rechteinhabers ist strafbar.
						</p>
						<h3>Quelltext</h3>
						<p>
							Der Quelltext dieser Seite ist auf <a href="http://git.bbnetz.eu/igruhrpottfotografie">BitBucket</a> frei verfügbar.
						</p>
					</article>
				</div>
			</div>

			<footer>
				<span id="facebook"></span>
				<div class="logo"></span>
			</footer>
		</div>
	</body>
</html>
